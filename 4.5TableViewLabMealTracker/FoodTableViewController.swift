//
//  FoodTableViewController.swift
//  4.5TableViewLabMealTracker
//  Page 533
//
//  Created by Amaan on 2017-11-23.
//  Copyright © 2017 Amaan. All rights reserved.
//

import UIKit

class FoodTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        //a) Return the number of meals in your meals array
        // TODO: Question - Do I need "self." for meals? Why or why not?
        return meals.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Access the meal for the given section, and return the number of food items in that meal
        return meals[section].food.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell", for: indexPath)
        //a) Dequeue a cell with the reuse identifier "foodCell."
        //b) Access the meal for the row using indexPath.section.
        //c) Access the food for the row using indexPath.row.
        //d) Update the cell's text label text and detail text label text to be the name and description of the food item.
        
        let food = meals[indexPath.section].food[indexPath.row]
        cell.textLabel?.text = food.name
        cell.detailTextLabel?.text = food.description
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return meals[section].name
    }
    // MARK: - Data
    
    var meals: [Meal] {
        //a) Create three Meal objects. Name them "breakfast," "lunch," and "dinner."
        //b) Create three Food objects for the food array on each Meal object. Give each Food item appropriate name for its corresponding meal.
        //c) Return the three Meal objects in an array.
        // TODO: Question - Best place to put this data and good naming convention for it? If code is read top to bottom, is it better to write this at the top?

        
        //breakfast Food
        let eggs = Food(name:"Eggs", description: "Protein")
        let orangeJuice = Food(name:"Orange Juice", description: "Drink")
        let toast = Food(name:"Toast", description: "Carbs")
        
        //lunch Food
        let salad = Food(name:"Salad", description: "Veggies")
        let water = Food(name:"Water", description: "Drink")
        let tofu = Food(name:"Tofu", description: "Protein")
        
        //dinner Food
        let rice = Food(name:"Rice", description: "Carbs")
        let stirFry = Food(name:"Stir Fry", description: "Veggies")
        let icecream = Food(name:"Icecream", description: "Dessert")
        
        //Meals
        let breakfast = Meal(name: "Breakfast", food: [eggs, orangeJuice, toast])
        let lunch = Meal(name: "Lunch", food: [salad, water, tofu])
        let dinner = Meal(name: "Dinner", food: [rice, stirFry, icecream])
        
        return [breakfast, lunch, dinner]
    }
    
}
